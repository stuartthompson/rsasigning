#pragma once

#ifndef RSASIGNING_SIGNEDMESSAGE_HPP
#define RSASIGNING_SIGNEDMESSAGE_HPP

#include <string>
#include <vector>

class SignedMessage
{
public:
	SignedMessage();
	SignedMessage(std::string message, std::vector<char> signature);
	~SignedMessage();

	// Loads a signed message from a file
	bool load(const std::string file_path);

	// Saves the current message and signature to a file
	bool save(const std::string file_path) const;

	// Formats the signature as a hex string
	std::string signature_hex(void) const;

	// Properties
	std::string get_message(void) const;
	std::vector<char> get_signature(void) const;
	
private:
	std::string message_;
	// Have to use a vector of char for signature since it can contain a 0 char
	// The zero char would cause a string to appear to terminate early (i.e. A0:40:00:B0 becomes A0:40 in a string)
	std::vector<char> signature_;
};

#endif
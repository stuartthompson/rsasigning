#pragma once

#ifndef RSASIGNING_MESSAGE_SIGNER_HPP
#define RSASIGNING_MESSAGE_SIGNER_HPP

#include <string>
#include <vector>

#include "SignedMessage.hpp"

#include "../inc/cryptlib/osrng.h"
#include "../inc/cryptlib/rsa.h"

class MessageSigner
{
public:
	// Create a new message signer and generate a new key
	MessageSigner();
	// Create a new message signer and generate a new key of the specified key size
	MessageSigner(unsigned int key_size);
	// Create a new message signer using a key loaded from the specified path
	MessageSigner(std::string key_file_path);
	// Create a new message signer using the supplied key
	MessageSigner(CryptoPP::InvertibleRSAFunction keys);

	// Save the key used by this class
	void save_key(const std::string key_file_path) const;
	// Sign a message and return a SignedMessage object
	SignedMessage sign_message(const std::string message);
	// Verify the signature on a signed message
	bool verify_signature(const SignedMessage signed_message) const;

private:
	CryptoPP::AutoSeededRandomPool rng_;
	CryptoPP::InvertibleRSAFunction keys_;
	CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA>::Signer signer_;
};

#endif
#include "stdafx.h"
#include "SignedMessage.hpp"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>

#include "../inc/cryptlib/hex.h"

#include "SigningException.hpp"

SignedMessage::SignedMessage()
{
}

SignedMessage::SignedMessage(std::string message, std::vector<char> signature)
{
	this->message_ = message;
	this->signature_ = signature;
}

SignedMessage::~SignedMessage()
{
}

// Loads a signed message from a file.
// Returns true if the message loaded correctly, false otherwise.
bool SignedMessage::load(const std::string file_path)
{
	// Verify file exists
	struct stat fileInfo;
	bool file_exists = stat(file_path.c_str(), &fileInfo) == 0;
	if (!file_exists)
		throw SigningException{ "Cannot load signed message. File not found at: " + file_path };
	
	// Open message file
	std::ifstream message_file;
	message_file.open(file_path, std::ifstream::in, std::ifstream::binary);

	int message_length = 0;
	int signature_length = 0;

	// First four bytes contain message length
	message_file.read((char*)&message_length, 4);
	// Next four bytes contain signature length
	message_file.read((char*)&signature_length, 4);

	// Next comes the message
	// Allocate empty string (that is large enough to read into)
	this->message_ = std::string(message_length, 0);
	// TODO: Must be a MUCH better way to do this
	// TODO: String streams?
	message_file.read(&this->message_[0], message_length);
	
	// Then comes the signature
	// Allocate a vector large enough to hold all bytes of signature
	this->signature_ = std::vector<char>(signature_length);
	// Read bytes into signature string
	message_file.read(&this->signature_[0], signature_length);
	
	// Close message file
	message_file.close();

	return true;
}

// Save this message to disk
bool SignedMessage::save(const std::string file_path) const
{
	// Verify that both a message and signature are specified
	if (this->message_.length() == 0)
		throw SigningException("Cannot save signed message. Message is empty.");
	
	if (this->signature_.size() == 0)
		throw SigningException("Cannot save signed message. Signature is empty.");
	
	std::ofstream message_file;
	message_file.open(file_path, std::ofstream::out, std::ofstream::binary);

	int message_length = this->message_.length();
	int signature_length = this->signature_.size();
	
	// First four bytes of file are message length
	message_file.write((char*)&message_length, 4);
	// Next four bytes are length of signature
	message_file.write((char*)&signature_length, 4);

	// Write message to file
	std::copy(
		this->message_.begin(),
		this->message_.end(),
		std::ostreambuf_iterator<char>(message_file));
	
	// Write signature to file
	std::copy(
		this->signature_.begin(), 
		this->signature_.end(), 
		std::ostreambuf_iterator<char>(message_file));

	message_file.close();
}

// Return the message signature formatted as a hex string
std::string SignedMessage::signature_hex(void) const
{
	std::string hex_signature;

	// Encode from signature vector to hex signature string
	CryptoPP::StringSource ss(
		(byte*)&this->signature_[0],				// where to read from
		this->signature_.size() * sizeof(byte),		// number of bytes to read
		true,										// pump all = true (don't buffer?)
		new CryptoPP::HexEncoder(
			new CryptoPP::StringSink(hex_signature),
			true,	// uppercase
			2,		// groups of two characters
			":",	// separated by a colon
			"")		// no terminator
		);

	return hex_signature;
}


/////////////
// Properties

// Get message
std::string SignedMessage::get_message(void) const
{
	return this->message_;
}

// Get signature
std::vector<char> SignedMessage::get_signature(void) const
{
	return this->signature_;
}
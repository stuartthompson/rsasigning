// RSASigning.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <fstream>
#include <iomanip>
#include <iostream>

#include "../inc/cryptlib/files.h"
#include "../inc/cryptlib/rsa.h"
#include "../inc/cryptlib/osrng.h"
#include "../inc/cryptlib/integer.h"
#include "../inc/cryptlib/sha.h"
#include "../inc/cryptlib/hex.h"
#include "../inc/cryptlib/filters.h"

#include "MessageSigner.hpp"
#include "SignedMessage.hpp"
#include "SigningException.hpp"
#include "TestRunner.hpp"

bool test_one(void);
bool test_two(void);
bool test_three(void);
bool test_four(void);
bool test_five(void);
bool test_six(void);
bool test_seven(void);
bool test_eight(void);

int main()
{
	TestRunner test_runner;

	test_runner.register_test("Sign and verify signature.", test_one);
	test_runner.register_test("Load signed message from file and verify signature.", test_two);
	test_runner.register_test("Key and signature don't match. Expect verification failure.", test_three);
	test_runner.register_test("Verify hex version of signature is generated correctly.", test_four);
	test_runner.register_test("Signing empty message results in SigningException.", test_five);
	test_runner.register_test("Attempting to load non-existant SignedMessage results in SigningException.", test_six);
	test_runner.register_test("Attempting to save SignedMessage with no message results in SigningException.", test_seven);
	test_runner.register_test("Attempting to save SignedMessage with no signature results in SigningException.", test_eight);

	test_runner.run_tests();

    return 0;
}

// Test One
// Sign a message and verify the signature.
bool test_one(void)
{
	MessageSigner message_signer;
	SignedMessage signed_message = message_signer.sign_message("{\"email\":\"testy.mctest@testing.com\"}");

	bool verified = message_signer.verify_signature(signed_message);

	return verified;
}

// Test Two
// Load a signed message from a file and verify the signature.
// The key for the message signer needs to be loaded in order for the signature to be verified.
bool test_two(void)
{
	MessageSigner message_signer("testdata/test2-key.dat");
	SignedMessage signed_message;

	// Load a signed message
	signed_message.load("testdata/test2-signedmessage.dat");

	// Verify signature
	bool verified = message_signer.verify_signature(signed_message);

	return verified;
}

// Test Three
// Negative test - key and signature don't match.
// Load a signed message from a file and verify the signature.
// Test is expected to fail since a new key is generated and the signature therefore should not match.
bool test_three(void)
{
	MessageSigner message_signer;
	SignedMessage signed_message;

	// Load a message that was previously signed
	signed_message.load("testdata/test3-signedmessage.dat");

	// Verify signature
	bool verified = message_signer.verify_signature(signed_message);

	// Verification should fail (since the key and signature won't match)
	return !verified;
}

// Test Four
// Load a signed message and verify that the hex encoded signature is as expected.
bool test_four(void)
{
	MessageSigner message_signer;
	SignedMessage signed_message;

	// Load a message that was previously signed
	signed_message.load("testdata/test4-signedmessage.dat");

	// Convert signature to hex
	std::string hex_sig = signed_message.signature_hex();

	bool verified = hex_sig == "97:6B:13:92:4E:B0:74:24:BA:6D:45:ED:DD:98:69:90:FA:BB:88:5A:32:2C:00:30:E2:5A:7C:9C:0C:E9:17:7C:9E:6D:C6:EC:D4:13:B3:DE:BC:AD:31:1E:2C:3C:9B:46:97:D4:6F:FE:42:A4:15:80:FE:2D:96:AC:30:37:6C:B7";
	
	return verified;
}

// Test Five
// Exception test.
// Attempting to sign an empty message should result in a SigningException.
bool test_five(void)
{
	MessageSigner message_signer;

	bool signing_exception_caught = false;

	try
	{
		SignedMessage signed_message = message_signer.sign_message("");
	}
	catch (SigningException& exception)
	{
		signing_exception_caught = true;
	}

	return signing_exception_caught;
}

// Test Six
// Exception test.
// Attempt to load message file that does not exist.
bool test_six(void)
{
	bool signing_exception_caught = false;

	try
	{
		SignedMessage signed_message;
		signed_message.load("doesntexist.dat");
	}
	catch (SigningException& exception)
	{
		signing_exception_caught = true;
	}

	return signing_exception_caught;
}

// Test Seven
// Exception test.
// Attempt to save a SignedMessage that doesn't not contain a message.
bool test_seven(void)
{
	bool signing_exception_caught = false;

	try
	{
		SignedMessage signed_message{ std::string{}, std::vector<char> {'n','o','t','a','s','i','g'} };
		signed_message.save("testdata/test7-signedmessage.dat"); // This shouldn't actually save a file
	}
	catch (SigningException& exception)
	{
		signing_exception_caught = true;
	}

	return signing_exception_caught;
}

// Test Eight
// Exception test.
// Attempt to save a SignedMessage that doesn't not contain a signature.
bool test_eight(void)
{
	bool signing_exception_caught = false;

	try
	{
		SignedMessage signed_message { "somemessage", std::vector<char>{} };
		signed_message.save("testdata/test8-signedmessage.dat"); // This shouldn't actually save a file
	}
	catch (SigningException& exception)
	{
		signing_exception_caught = true;
	}

	return signing_exception_caught;
}
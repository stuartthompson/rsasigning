#include "stdafx.h"
#include "MessageSigner.hpp"

#include "SigningException.hpp"

#include "../inc/cryptlib/files.h"

MessageSigner::MessageSigner()
{
	this->keys_.GenerateRandomWithKeySize(this->rng_, 512);
	this->signer_ = CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA>::Signer(this->keys_);
}

MessageSigner::MessageSigner(unsigned int key_size)
{
	this->keys_.GenerateRandomWithKeySize(this->rng_, key_size);
	this->signer_ = CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA>::Signer(this->keys_);
}

MessageSigner::MessageSigner(std::string key_file_path)
{
	CryptoPP::FileSource key_file(key_file_path.c_str(), true);
	this->keys_.BERDecode(key_file);
	this->signer_ = CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA>::Signer(this->keys_);
}

MessageSigner::MessageSigner(CryptoPP::InvertibleRSAFunction keys)
{
	this->keys_ = keys;
	this->signer_ = CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA>::Signer(this->keys_);
}

// Save the key used by this signer
void MessageSigner::save_key(const std::string key_file_path) const
{
	CryptoPP::FileSink key_file(key_file_path.c_str());
	this->keys_.DEREncode(key_file);
}

// Sign a message and return a SignedMessage object
SignedMessage MessageSigner::sign_message(const std::string message)
{
	// Can't (or shouldn't) sign an empty message
	if (message == "")
		throw SigningException("Attempt to sign an empty message.");
	
	// Sign message
	byte* sig = new byte[this->signer_.MaxSignatureLength()];
	size_t sig_length = this->signer_.SignMessage(this->rng_, (const byte*)message.c_str(), message.length(), sig);

	// Store the signature in a vector
	std::vector<char> signature(sig, sig + sig_length);

	// Create and return signed message
	SignedMessage signed_message(message, signature);
	return signed_message;
}

bool MessageSigner::verify_signature(const SignedMessage signed_message) const
{
	// Build signer and verifier based upon supplied keys
	CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA>::Verifier verifier(this->signer_);

	// Verify message
	return verifier.VerifyMessage(
		(const byte*)signed_message.get_message().c_str(),	// Message
		signed_message.get_message().length(),				// Length of message
		(const byte*)&signed_message.get_signature()[0],	// Signature
		signed_message.get_signature().size());				// Length of signature
}
#include "stdafx.h"
#include "TestRunner.hpp"

#include <iomanip>
#include <iostream>
#include <sstream>

TestRunner::TestRunner()
{
	this->tests_run_ = 0;
	this->tests_passed_ = 0;
}

TestRunner::~TestRunner()
{
}

void TestRunner::register_test(const std::string name, bool(*func)(void))
{
	this->tests_.push_back(std::pair<std::string, bool(*)(void)>{name, func});
}

void TestRunner::run_tests()
{
	// Print startup
	std::cout << "Testing started at " << this->get_datestamp() << " " << this->get_timestamp() << std::endl << std::endl;

	// Run tests
	for (size_t i = 0; i < this->tests_.size(); i++)
	{
		auto test = this->tests_[i];
		this->run_test(test.first, test.second);
	}

	// Print summary
	this->print_summary();
}

void TestRunner::run_test(const std::string name, bool(*func)(void))
{
	this->tests_run_++;
	bool passed = func();

	if (passed)
	{
		this->tests_passed_++;
	}

	std::cout << "Test " << this->tests_run_ << " (" << (passed ? "PASSED" : "FAILED") << ")" << ": " << name << std::endl;
}

bool TestRunner::all_passed(void)
{
	return this->tests_passed_ == this->tests_run_;
}

int TestRunner::num_tests_run(void)
{
	return this->tests_run_;
}

int TestRunner::num_tests_passed(void)
{
	return this->tests_passed_;
}

int TestRunner::num_tests_failed(void)
{
	return this->tests_run_ - this->tests_passed_;
}

void TestRunner::print_summary(void)
{
	std::cout << std::endl;
	std::cout << "  Tests: " << std::to_string(this->num_tests_run()) << std::endl;
	std::cout << " Passed: " << std::to_string(this->num_tests_passed()) << std::endl;
	std::cout << " Failed: " << std::to_string(this->num_tests_failed()) << std::endl;
}

// Gets the current datestamp.
std::string TestRunner::get_datestamp() const
{
	std::time_t t = time(0);
	struct tm now;
	localtime_s(&now, &t);

	std::ostringstream oss;

	int daynum = now.tm_mday;
	int monthnum = now.tm_mon + 1;

	oss << (now.tm_year + 1900) << '-';
	if (monthnum < 10) oss << '0'; 	// Pad month with leading zero, if necessary
	oss << monthnum << '-';
	if (daynum < 10) oss << '0';	// Pad day with leading zero, if necessary
	oss << daynum;

	return oss.str();
}

// Gets the current timestamp.
std::string TestRunner::get_timestamp() const
{
	std::time_t t = time(0);
	struct tm now;
	localtime_s(&now, &t);

	std::ostringstream oss;

	int hour = now.tm_hour;
	int minutes = now.tm_min;
	int seconds = now.tm_sec;

	if (hour < 10) oss << '0';		// Pad hour, if necessary
	oss << hour << ':';
	if (minutes < 10) oss << '0';	// Pad minutes, if necessary
	oss << minutes << ':';
	if (seconds < 10) oss << '0';	// Pad seconds, if necessary
	oss << seconds;

	return oss.str();
}
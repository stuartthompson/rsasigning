#include "stdafx.h"
#include "SigningException.hpp"

SigningException::SigningException(const std::string message)
{
	this->message_ = message;
}

const char* SigningException::what() const throw()
{
	return this->message_.c_str();
}
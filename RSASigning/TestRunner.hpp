#pragma once

#ifndef RSASIGNING_TESTRUNNER_HPP
#define RSASIGNING_TESTRUNNER_HPP

#include <string>
#include <vector>

class TestRunner
{
public:
	TestRunner();
	~TestRunner();

	void register_test(const std::string name, bool(*func)(void));
	void run_tests();

	bool all_passed(void);
	int num_tests_run(void);
	int num_tests_passed(void);
	int num_tests_failed(void);

private:
	void run_test(const std::string name, bool(*func)(void));
	void print_summary(void);
	std::string get_datestamp() const;
	std::string get_timestamp() const;

	std::vector<std::pair<std::string, bool(*)(void)>> tests_;
	int tests_run_;
	int tests_passed_;
};

struct Test
{

};

#endif
#pragma once

#ifndef RSASIGNING_SIGNINGEXCEPTION_HPP
#define RSASIGNING_SIGNINGEXCEPTION_HPP

#include <exception>
#include <string>

class SigningException : public std::exception
{
public:
	SigningException(const std::string message);

	virtual const char* what() const throw();
private:
	std::string message_;
};

#endif
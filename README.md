# RSASigning
Researching how to sign messages to be transmitted between a server and client.
This project is purely to learn how the CryptoPP library works and to explore
structures to be used in other projects, such as Progeny.

## Testing
The main project comes with a set of tests that exercise different paths 
through the code.

### Resources
Article to check out for a public/private key example:
http://marko-editor.com/articles/cryptopp_sign_string/